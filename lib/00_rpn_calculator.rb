class RPNCalculator

  def initialize
    @stack = []
  end


  def push(item)
    @stack << item
  end


  def calculate_and_update(operation)
    raise "calculator is empty" if @stack.empty?
    num1 = @stack.pop
    num2 = @stack.pop

    resulting_number =
      case operation
      when :+ then num2 + num1
      when :- then num2 - num1
      when :* then num2 * num1
      when :/ then num2.to_f / num1
      end

    @stack << resulting_number
  end#of method


  def plus
    calculate_and_update(:+)
  end


  def minus
    calculate_and_update(:-)
  end


  def times
    calculate_and_update(:*)
  end


  def divide
    calculate_and_update(:/)
  end


  def value
    @stack[-1]
  end


  def tokens(str)
    symbols = [:+, :-,:*, :/]
    str.split(/\s+/).map { |item| symbols.include?(item.to_sym) ? item.to_sym : item.to_i }
  end


  def evaluate(str)
    array_of_tokens = tokens(str)

    array_of_tokens.each do |token|
      if token.class == Integer
        push(token)
      else
        calculate_and_update(token)
      end
    end

    @stack[-1]
  end#of method


end#of class
